package ltd.hello.admin.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 发布需求
 * </p>
 *
 * @author trik
 * @since 2021-04-09
 */
@Data
  @EqualsAndHashCode(callSuper = false)
    @TableName("orders")
@ApiModel(value="Orders对象", description="发布需求")
public class Orders implements Serializable {

    private static final long serialVersionUID=1L;

      @TableId(value = "Id", type = IdType.AUTO)
      private Integer Id;

      @ApiModelProperty(value = "检索关键字")
      private String keyWords;

      @ApiModelProperty(value = "联系方式")
      private String contactInfo;

      @ApiModelProperty(value = "类型（毕业生，专家，设备，平台，技术..）")
      private String orderType;

      @ApiModelProperty(value = "工作地点")
      private String workAddress;

      @ApiModelProperty(value = "企业名称")
      private String compName;

      @ApiModelProperty(value = "标题")
      private String orderTitle;

      @ApiModelProperty(value = "发布时间")
      private Date issueDate;

      @ApiModelProperty(value = "联系人")
      private String contactPerson;

      @ApiModelProperty(value = "具体要求")
      private String orderContent;

      @ApiModelProperty(value = "研究领域")
      private String reseachField;

      @ApiModelProperty(value = "所需人数")
      private String orderQuantity;

    private String remark;

    private String recorder;


}
