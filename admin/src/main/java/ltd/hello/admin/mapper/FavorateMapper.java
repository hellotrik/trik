package ltd.hello.admin.mapper;

import ltd.hello.admin.model.Favorate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 收藏 Mapper 接口
 * </p>
 *
 * @author trik
 * @since 2021-04-09
 */
public interface FavorateMapper extends BaseMapper<Favorate> {

}
