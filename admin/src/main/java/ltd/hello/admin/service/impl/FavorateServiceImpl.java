package ltd.hello.admin.service.impl;

import ltd.hello.admin.model.Favorate;
import ltd.hello.admin.mapper.FavorateMapper;
import ltd.hello.admin.service.FavorateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 收藏 服务实现类
 * </p>
 *
 * @author trik
 * @since 2021-04-09
 */
@Service
public class FavorateServiceImpl extends ServiceImpl<FavorateMapper, Favorate> implements FavorateService {

}
