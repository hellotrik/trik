package ltd.hello.trik.service;


import com.baomidou.mybatisplus.extension.service.IService;
import ltd.hello.trik.model.LogEntity;

public interface LogService extends IService<LogEntity> {

}
