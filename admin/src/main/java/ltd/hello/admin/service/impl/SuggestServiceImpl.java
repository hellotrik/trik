package ltd.hello.admin.service.impl;

import ltd.hello.admin.model.Suggest;
import ltd.hello.admin.mapper.SuggestMapper;
import ltd.hello.admin.service.SuggestService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author trik
 * @since 2021-04-09
 */
@Service
public class SuggestServiceImpl extends ServiceImpl<SuggestMapper, Suggest> implements SuggestService {

}
