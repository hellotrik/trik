package ltd.hello.admin.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 设备 前端控制器
 * </p>
 *
 * @author trik
 * @since 2021-04-09
 */
@RestController
@RequestMapping("/admin/equipment")
public class EquipmentController {

}

