package ltd.hello.admin.service.impl;

import ltd.hello.admin.model.Innovation;
import ltd.hello.admin.mapper.InnovationMapper;
import ltd.hello.admin.service.InnovationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author trik
 * @since 2021-04-09
 */
@Service
public class InnovationServiceImpl extends ServiceImpl<InnovationMapper, Innovation> implements InnovationService {

}
