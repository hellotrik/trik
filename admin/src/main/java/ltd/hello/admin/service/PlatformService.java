package ltd.hello.admin.service;

import ltd.hello.admin.model.Platform;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 平台 服务类
 * </p>
 *
 * @author trik
 * @since 2021-04-09
 */
public interface PlatformService extends IService<Platform> {

}
