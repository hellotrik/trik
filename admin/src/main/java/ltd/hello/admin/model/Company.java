package ltd.hello.admin.model;

import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author trik
 * @since 2021-04-09
 */
@Data
  @EqualsAndHashCode(callSuper = false)
    @TableName("company")
@ApiModel(value="Company对象", description="")
public class Company implements Serializable {

    private static final long serialVersionUID=1L;

    private String id;

    private String shortName;

    private String name;

    private Double lng;

    private Double lat;

    private String address;

    private Date createTime;

    private String legalPerson;

    private String compType;

    private String envirPerson;

    private String envirLevel;

    private String compTel;

    private String remark;

    private String envirCode;

    private Integer province;

    private Integer city;

    private Integer area;


}
