package ltd.hello.admin.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author trik
 * @since 2021-04-09
 */
@Data
  @EqualsAndHashCode(callSuper = false)
    @TableName("expert")
@ApiModel(value="Expert对象", description="")
public class Expert implements Serializable {

    private static final long serialVersionUID=1L;

      @TableId(value = "Id", type = IdType.AUTO)
      private Integer Id;

      @ApiModelProperty(value = "姓名")
      private String name;

      @ApiModelProperty(value = "生日")
      private Date birthday;

      @ApiModelProperty(value = "性别")
      private Integer gender;

      @ApiModelProperty(value = "个人照片")
      private String expertPic;

      @ApiModelProperty(value = "研究领域")
      private String researchField;

      @ApiModelProperty(value = "职务")
      private String subject;

      @ApiModelProperty(value = "学历")
      private String degree;

      @ApiModelProperty(value = "毕业院校")
      private String graduateInstitutions;

      @ApiModelProperty(value = "专业主修")
      private String major;

      @ApiModelProperty(value = "工作单位")
      private String workOrg;

      @ApiModelProperty(value = "所在部门")
      private String workDept;

    private String workPos;

      @ApiModelProperty(value = "联系方式")
      private String mobile;

      @ApiModelProperty(value = "地址")
      private String address;

      @ApiModelProperty(value = "专业特长")
      private String specialties;

      @ApiModelProperty(value = "所获奖励")
      private String achievement;

      @ApiModelProperty(value = "教育经历")
      private String eduExperience;

      @ApiModelProperty(value = "创建时间")
      private Date createTime;

      @ApiModelProperty(value = "个人简介")
      private String remark;

    private String recorder;


}
