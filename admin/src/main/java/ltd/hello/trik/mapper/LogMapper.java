package ltd.hello.trik.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import ltd.hello.trik.model.LogEntity;

public interface LogMapper extends BaseMapper<LogEntity> {

}
