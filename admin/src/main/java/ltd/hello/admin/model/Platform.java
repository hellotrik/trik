package ltd.hello.admin.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 平台
 * </p>
 *
 * @author trik
 * @since 2021-04-09
 */
@Data
  @EqualsAndHashCode(callSuper = false)
    @TableName("platform")
@ApiModel(value="Platform对象", description="平台")
public class Platform implements Serializable {

    private static final long serialVersionUID=1L;

      @TableId(value = "Id", type = IdType.AUTO)
      private Integer Id;

      @ApiModelProperty(value = "名称")
      private String name;

      @ApiModelProperty(value = "平台编号")
      private String platNo;

      @ApiModelProperty(value = "批准部门")
      private String approvalDept;

      @ApiModelProperty(value = "平台类型")
      private String platType;

      @ApiModelProperty(value = "平台简介")
      private String platIntro;

      @ApiModelProperty(value = "检索关键字")
      private String keyWord;

      @ApiModelProperty(value = "负责人")
      private String platLeader;

      @ApiModelProperty(value = "联系人")
      private String linkman;

      @ApiModelProperty(value = "联系电话")
      private String telphone;

      @ApiModelProperty(value = "网址")
      private String website;

    private String email;

      @ApiModelProperty(value = "所属部门")
      private String platOrg;

      @ApiModelProperty(value = "登记时间")
      private Date createTime;

      @ApiModelProperty(value = "平台注册时间")
      private String regDate;

      @ApiModelProperty(value = "备注")
      private String remark;

    private String recorder;


}
