package ltd.hello.trik.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class MapUtil {
	public static List<?> getByLikeKey(Map<String, ?> map, String keyLike) {
		List<Object> list = new ArrayList<>();
		for (Map.Entry<String, ?> entity : map.entrySet()) {
			if (entity.getKey().indexOf(keyLike) > -1) {
				list.add(entity.getValue());
			}
		}
		return list;
	}

	public static List<?> getKeyByLikeKey(Map<String, ?> map, String keyLike) {
		List<Object> list = new ArrayList<>();
		for (Map.Entry<String, ?> entity : map.entrySet()) {
			if (entity.getKey().indexOf(keyLike) > -1) {
				list.add(entity.getKey());
			}
		}
		return list;
	}

	public static void main(String[] args) {
		Map<String, Object> map = new ConcurrentHashMap<String, Object>();
		map.put("safein", "safein");
		map.put("safein1tebg23", "safein23");
		map.put("1_twjsafein123", "twjsafein1233");
		map.put("1_twj", "twj");
		List<?> list = getByLikeKey(map, "twj");
		for (Object val : list) {
			System.err.println(val.toString());
		}
	}
}
