package ltd.hello.admin.mapper;

import ltd.hello.admin.model.Equipment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 设备 Mapper 接口
 * </p>
 *
 * @author trik
 * @since 2021-04-09
 */
public interface EquipmentMapper extends BaseMapper<Equipment> {

}
