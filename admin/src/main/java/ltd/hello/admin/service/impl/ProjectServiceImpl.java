package ltd.hello.admin.service.impl;

import ltd.hello.admin.model.Project;
import ltd.hello.admin.mapper.ProjectMapper;
import ltd.hello.admin.service.ProjectService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author trik
 * @since 2021-04-09
 */
@Service
public class ProjectServiceImpl extends ServiceImpl<ProjectMapper, Project> implements ProjectService {

}
