package ltd.hello.trik.util;

public class Convert {

	public Convert() {
	}

	// 车牌颜色代码转名称
	public static String color2Name(int code) {
		String ret = "未知";
		switch (code) {
		case 1:
			ret = "蓝色";
			break;
		case 2:
			ret = "黄色";
			break;
		case 3:
			ret = "黑色";
			break;
		case 4:
			ret = "白色";
			break;
		case 9:
			ret = "其他";
			break;
		default:
			break;
		}
		return ret;
	}

	public static int color2Id(String colorname) {
		int i;
		if (colorname.equals("蓝色")) {
			i = 1;

		} else if (colorname.equals("黄色")) {
			i = 2;
		} else if (colorname.equals("黑色")) {
			i = 3;
		} else if (colorname.equals("白色")) {
			i = 4;
		} else {
			i = 9;
		}
		return i;
	}

	public static String compType2Name(int code) {
		String ret = "未知";
		switch (code) {
		case 1:
			ret = "接驳运输联盟";
			break;
		case 2:
			ret = "接驳运输企业";
			break;
		default:
			break;
		}
		return ret;
	}

	public static String userType2Name(int code) {
		String ret = "未知";
		switch (code) {
		case 0:
			ret = "系统管理员";
			break;
		case 1:
			ret = "高级管理员";
			break;
		case 2:
			ret = "中级管理员";
			break;
		case 3:
			ret = "网格员";
			break;
		default:
			break;
		}
		return ret;
	}

	public static String degree2Name(int code) {
		String ret = "未知";
		switch (code) {
		case 10:
			ret = "研究生及以上";
			break;
		case 20:
			ret = "大学本科";
			break;
		case 30:
			ret = "大学专科";
			break;
		case 40:
			ret = "中专技校";
			break;
		case 60:
			ret = "普通高中";
			break;
		case 70:
			ret = "初中及以下";
			break;
		case 90:
			ret = "其他";
			break;
		default:
			break;
		}
		return ret;
	}

	public static String identityId2Name(int code) {
		String ret = "未知";
		switch (code) {
		case 1:
			ret = "居民身份证";
			break;
		case 2:
			ret = "军官证";
			break;
		case 3:
			ret = "护照";
			break;
		case 4:
			ret = "机动车驾驶证";
			break;
		case 5:
			ret = "港澳通行证";
			break;
		case 6:
			ret = "台胞证";
			break;
		case 9:
			ret = "其他国家认可的有效证件";
			break;
		default:
			break;
		}
		return ret;
	}

	public static String nationId2Name(int code) {
		String ret = "未知";
		switch (code) {
		case 1:
			ret = "汉族";
			break;
		case 2:
			ret = "蒙古族";
			break;
		case 3:
			ret = "回族";
			break;
		case 4:
			ret = "藏族";
			break;
		case 5:
			ret = "维吾尔族";
			break;
		case 6:
			ret = "苗族";
			break;
		case 7:
			ret = "彝族";
			break;
		case 8:
			ret = "壮族";
			break;
		case 9:
			ret = "布依族";
			break;
		case 10:
			ret = "朝鲜族";
			break;
		case 11:
			ret = "满族";
			break;
		case 12:
			ret = "侗族";
			break;
		case 13:
			ret = "瑶族";
			break;
		case 14:
			ret = "白族";
			break;
		case 15:
			ret = "土家族";
			break;
		case 16:
			ret = "哈尼族";
			break;
		case 17:
			ret = "哈萨克族";
			break;
		case 18:
			ret = "傣族";
			break;
		case 19:
			ret = "黎族";
			break;
		case 20:
			ret = "傈僳族";
			break;
		case 21:
			ret = "佤族";
			break;
		case 22:
			ret = "畲族";
			break;
		case 23:
			ret = "高山族";
			break;
		case 24:
			ret = "拉祜族";
			break;
		case 25:
			ret = "水族";
			break;
		case 26:
			ret = "东乡族";
			break;
		case 27:
			ret = "纳西族";
			break;
		case 28:
			ret = "景颇族";
			break;
		case 29:
			ret = "柯尔克孜族";
			break;
		case 30:
			ret = "土族";
			break;
		case 31:
			ret = "达斡尔族";
			break;
		case 32:
			ret = "仫佬族";
			break;
		case 33:
			ret = "羌族";
			break;
		case 34:
			ret = "布朗族";
			break;
		case 35:
			ret = "撒拉族";
			break;
		case 36:
			ret = "毛难族";
			break;
		case 37:
			ret = "仡佬族";
			break;
		case 38:
			ret = "锡伯族";
			break;
		case 39:
			ret = "阿昌族";
			break;
		case 40:
			ret = "普米族";
			break;
		case 41:
			ret = "塔吉克族";
			break;
		case 42:
			ret = "怒族";
			break;
		case 43:
			ret = "乌孜别克族";
			break;
		case 44:
			ret = "俄罗斯族";
			break;
		case 45:
			ret = "鄂温克族";
			break;
		case 46:
			ret = "崩龙族";
			break;
		case 47:
			ret = "保安族";
			break;
		case 48:
			ret = "裕固族";
			break;
		case 49:
			ret = "京族";
			break;
		case 50:
			ret = "塔塔尔族";
			break;
		case 51:
			ret = "独龙族";
			break;
		case 52:
			ret = "鄂伦春族";
			break;
		case 53:
			ret = "赫哲族";
			break;
		case 54:
			ret = "门巴族";
			break;
		case 55:
			ret = "珞巴族";
			break;
		case 56:
			ret = "基诺族";
			break;
		case 97:
			ret = "其他";
			break;
		case 98:
			ret = "外国血统中国籍人士";
			break;
		default:
			break;
		}
		return ret;
	}

	// 企业经营状态
	public static String operateStatus2Name(int code) {
		String ret = "未知";
		switch (code) {
		case 1:
			ret = "营业";
			break;
		case 2:
			ret = "停业";
			break;
		case 3:
			ret = "整改";
			break;
		case 4:
			ret = "停业整顿";
			break;
		case 5:
			ret = "歇业";
			break;
		case 6:
			ret = "注销";
			break;
		case 9:
			ret = "其他";
			break;
		default:
			break;
		}
		return ret;
	}

	// 审核状态
	public static String checkPass2Name(int code) {
		String ret = "未知";
		switch (code) {
		case 1:
			ret = "未通过";
			break;
		case 2:
			ret = "通过";
			break;
		case 10:
			ret = "未审核";
			break;
		default:
			break;
		}
		return ret;
	}
}
