package ltd.hello.trik.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import ltd.hello.trik.mapper.WxuserMapper;
import ltd.hello.trik.model.Wxuser;
import ltd.hello.trik.service.WxuserService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author nianyy
 * @since 2019-11-13
 */
@Service
public class WxuserServiceImpl extends ServiceImpl<WxuserMapper, Wxuser> implements WxuserService {

    @Override
    public Wxuser selectByOpenId(String openid) {
        QueryWrapper<Wxuser> eww = new QueryWrapper<>();
        eww.lambda().eq(Wxuser::getOpenid, openid);
        return this.baseMapper.selectOne(eww);
    }
}
