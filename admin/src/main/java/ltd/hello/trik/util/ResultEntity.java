package ltd.hello.trik.util;

import java.util.HashMap;

public class ResultEntity extends HashMap<String, Object> {

	private static final long serialVersionUID = -2021709832775627390L;

	public ResultEntity() {
		this.put("success", false);
	}

	public ResultEntity(boolean flag) {
		this.put("success", flag);
	}

	public ResultEntity(boolean flag, String errorMsg) {
		this.put("success", flag);
		this.put("errorMsg", errorMsg);
	}

	public ResultEntity(Object result) {
		if (result == null) {
			this.put("success", false);
			this.put("errorMsg", "未查到数据，请检查输入条件");
		} else {

			this.put("success", true);
			this.put("data", result);
			if (result instanceof Pageable) {
				Pageable page = (Pageable) result;
				this.put("code", 0);
				this.put("msg", "数据获取正常");
				this.put("count", page.getTotal());
				this.put("data1", page.getList());
			}

		}
	}

	public ResultEntity(int count) {
		if (count > 0) {
			this.put("success", true);
		} else {
			this.put("success", false);
			this.put("errorMsg", "操作失败");
		}
	}

//	public ResultEntity(Exception e) {
//		e.printStackTrace();
//		this.put("success", false);
//		this.put("errorMsg", "系统异常，请联系管理员！");
//	}

	public void setResultEntity(boolean flag, Object data) {
		this.put("success", flag);
		this.put("data", data);
	}

	public void setResultEntity(int count) {
		if (count > 0) {
			this.put("success", true);
		} else {
			this.put("success", false);
			this.put("errorMsg", "操作失败");
		}
	}

	public void setResultEntity(Object result) {
		if (result == null) {
			this.put("success", false);
			this.put("errorMsg", "未查到数据，请检查输入条件");
		} else {
			this.put("success", true);
			this.put("data", result);
		}
	}

	/**
	 * layui 通用返回 resultmap.put("code", 0); resultmap.put("msg", "数据获取正常");
	 * resultmap.put("count",
	 * Integer.parseInt(resultData.get("total").toString()));
	 * resultmap.put("data", resultData.get("list"));
	 */
	public ResultEntity(int code, String msg, int count, Object data) {
		if (data == null) {
			this.put("code", 100);
			this.put("msg", "数据获取异常");
			this.put("count", 0);
			this.put("data", null);
		} else {
			this.put("code", 0);
			this.put("msg", "数据获取正常");
			this.put("count", count);
			this.put("data", data);
		}
	}
}
