package ltd.hello.admin.service.impl;

import ltd.hello.admin.model.Device;
import ltd.hello.admin.mapper.DeviceMapper;
import ltd.hello.admin.service.DeviceService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author trik
 * @since 2021-04-09
 */
@Service
public class DeviceServiceImpl extends ServiceImpl<DeviceMapper, Device> implements DeviceService {

}
