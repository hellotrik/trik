package ltd.hello.trik.util;

import java.io.Serializable;
import java.util.HashMap;

public class SearchMap extends HashMap<String, Object> implements Serializable {

	private static final long serialVersionUID = -171461888820868674L;

	/**
	 *
	 * @param pageIndex
	 *            页码
	 * @param pageSize
	 *            每页显示的记录数
	 * @param obrderby
	 *            排序字段及方弿如："id desc"
	 */
	public SearchMap(Integer pageIndex, Integer pageSize, String obrderby) {
		if (pageIndex == null) {
			pageIndex = 1;
		}
		if (pageSize == null) {
			pageSize = 10;
		}
		this.put("pageIndex", pageIndex);
		this.put("pageSize", pageSize);
		this.put("startIndex", (pageIndex - 1) * pageSize);
		this.put("endIndex", pageIndex * pageSize);
		this.put("orderby", obrderby);
	}

	/**
	 * 设置查询条件：开始时闿
	 *
	 * @param begintime
	 */
	public void setBegintime(String begintime) {
		this.put("begintime", begintime);
	}

	/**
	 * 设置查询条件：结束时闿
	 *
	 * @param endtime
	 */
	public void setEndtime(String endtime) {
		this.put("endtime", endtime);
	}

	/**
	 * 设置查询条件：名秿
	 *
	 * @param endtime
	 */
	public void setName(String name) {
		this.put("name", name);
	}

	/**
	 * 设置其他查询条件
	 *
	 * @param endtime
	 */
	public void setParameter(String key, Object value) {
		this.put(key, value);
	}
}
