package ltd.hello.admin.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author trik
 * @since 2021-04-09
 */
@Data
  @EqualsAndHashCode(callSuper = false)
    @TableName("suggest")
@ApiModel(value="Suggest对象", description="")
public class Suggest implements Serializable {

    private static final long serialVersionUID=1L;

      @TableId(value = "Id", type = IdType.AUTO)
      private Integer Id;

    private String openid;

    private String content;

    private Date createTime;


}
