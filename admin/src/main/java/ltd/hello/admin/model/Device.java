package ltd.hello.admin.model;

import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author trik
 * @since 2021-04-09
 */
@Data
  @EqualsAndHashCode(callSuper = false)
    @TableName("device")
@ApiModel(value="Device对象", description="")
public class Device implements Serializable {

    private static final long serialVersionUID=1L;

    private String id;

    private String deviceId;

    private String password;

    private String compId;

    private String name;

    private Double lng;

    private Double lat;

    private String address;

    private Date creatDate;

    private String onLine;

    private String state;

    private String manufacturer;

    private String model;

    private String remark;

    private Integer pollType;


}
