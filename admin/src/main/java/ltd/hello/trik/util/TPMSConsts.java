package ltd.hello.trik.util;

import java.nio.charset.Charset;

public class TPMSConsts {

	public static final String encoding_gbk = "GBK";
	public static final String encoding_utf8 = "UTF-8";
	public static final String encoding_gb2312 = "GB2312";

	public static final Charset charset_gbk = Charset.forName(encoding_gbk);
	public static final Charset charset_utf8 = Charset.forName(encoding_utf8);
	public static final Charset charset_gb2312 = Charset.forName(encoding_gb2312);

}
