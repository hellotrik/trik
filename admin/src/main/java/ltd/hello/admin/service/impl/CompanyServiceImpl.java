package ltd.hello.admin.service.impl;

import ltd.hello.admin.model.Company;
import ltd.hello.admin.mapper.CompanyMapper;
import ltd.hello.admin.service.CompanyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author trik
 * @since 2021-04-09
 */
@Service
public class CompanyServiceImpl extends ServiceImpl<CompanyMapper, Company> implements CompanyService {

}
