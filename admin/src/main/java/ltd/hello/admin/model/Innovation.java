package ltd.hello.admin.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author trik
 * @since 2021-04-09
 */
@Data
  @EqualsAndHashCode(callSuper = false)
    @TableName("innovation")
@ApiModel(value="Innovation对象", description="")
public class Innovation implements Serializable {

    private static final long serialVersionUID=1L;

      @TableId(value = "Id", type = IdType.AUTO)
      private Integer Id;

    private String name;

      @ApiModelProperty(value = "研究人员")
      private String designer;

      @ApiModelProperty(value = "专利内容")
      private String innoContent;

      @ApiModelProperty(value = "专利类型")
      private String innoType;

      @ApiModelProperty(value = "公开日期")
      private Date openDate;

      @ApiModelProperty(value = "申请日期")
      private String applyDate;

      @ApiModelProperty(value = "申报组织")
      private String applyOrg;

      @ApiModelProperty(value = "联系方式")
      private String contactInfo;

      @ApiModelProperty(value = "备注")
      private String remark;

      @ApiModelProperty(value = "专利号")
      private String openNo;

      @ApiModelProperty(value = "主要负责人")
      private String responsiblePerson;

      @ApiModelProperty(value = "登记时间")
      private String createTime;

      @ApiModelProperty(value = "标识")
      private String flag;

    private String recorder;


}
