package ltd.hello.trik.util;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class DateUtil {

	public static final SimpleDateFormat NORM_DATETIME_FORMAT = new SimpleDateFormat("yyyyMMdd");
	public static final SimpleDateFormat NORM_DATEMONTH_FORMAT = new SimpleDateFormat("yyyyMM");
	public static final SimpleDateFormat NORM_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static final SimpleDateFormat BCD_NORM_DATE_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss");
	public static final SimpleDateFormat NORMDATEFORMAT = new SimpleDateFormat("yyyyMMddHHmmss");
	public static final SimpleDateFormat HHMMSS_DATE_FORMAT = new SimpleDateFormat("HHmmss");
	public static final SimpleDateFormat HH_MMSS_DATE_FORMAT = new SimpleDateFormat("HH:mm:ss");
	public static final SimpleDateFormat HH_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH");
	public static final SimpleDateFormat YYYY_MM_DD_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

	public static final SimpleDateFormat YYYY_MM_DD_CHINESE_FORMAT = new SimpleDateFormat("yyyy年MM月dd日");

	public static final SimpleDateFormat YYYY_MM_CHINESE_FORMAT = new SimpleDateFormat("yyyy年MM月");

	public static String getTableName(String dateday) throws ParseException {
		Date date = new Date();
		Calendar cal = Calendar.getInstance();
		if ("".equals(dateday)) {
			date = cal.getTime();
		} else {
			date = NORM_DATETIME_FORMAT.parse(dateday);
			cal.setTime(date);
		}
		StringBuffer sb = new StringBuffer();
		sb.append("csht_position_");
		// 先把字符串转成Date类型
		String tableName = NORM_DATEMONTH_FORMAT.format(date);
		sb.append(tableName);
		return sb.toString();
	}

	public static String dateToStr(Date date) {

		return NORM_DATE_FORMAT.format(date);
	}

	public static Date strToDate(String date) throws ParseException {
		return NORM_DATE_FORMAT.parse(date);
	}

	public static String dateToStr(SimpleDateFormat sdf, Date date) {
		return sdf.format(date);
	}

	public static Date dateToStr(SimpleDateFormat sdf, String date) throws Exception {
		Date d = null;
		d = sdf.parse(date);
		return d;
	}

	public static String dateLongToStr(long date) {
		Date d = new Date(date);
		return NORM_DATE_FORMAT.format(d);
	}

	public static String dateLongToStrHHMMSS(long date) {
		Date d = new Date(date);
		return HHMMSS_DATE_FORMAT.format(d);
	}

	public static String DateToStrHH_MMSS() {
		Date d = new Date();
		return HH_MMSS_DATE_FORMAT.format(d);
	}

	public static String longToBcdStr(long date) {
		if (date == 0) {
			return "000000000000";
		}
		Date d = new Date(date);
		return BCD_NORM_DATE_FORMAT.format(d);
	}

	/**
	 *
	 * @param time 格式：yyMMddHHmmss
	 * @return yyyy-MM-dd HH:mm:ss
	 */
	public static String changeFormat(String time) {
		Date date = null;
		String dateStr = "";
		try {
			date = BCD_NORM_DATE_FORMAT.parse(time);
			dateStr = NORM_DATE_FORMAT.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dateStr;
	}

	public static String changeFormat1(String time) {
		Date date = null;
		String dateStr = "";
		try {
			date = NORMDATEFORMAT.parse(time);
			dateStr = NORM_DATE_FORMAT.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dateStr;
	}

	/**
	 *
	 * @param time yyyy-MM-dd HH:mm:ss
	 *
	 * @return yyMMddHHmmss
	 */
	public static String changeBcdFormat(String time) {
		Date date = null;
		String dateStr = "";
		try {
			if("".equals(time) || time == null) {
				return "";
			}
			date = NORM_DATE_FORMAT.parse(time);
			dateStr = BCD_NORM_DATE_FORMAT.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dateStr;
	}

	/**
	 *
	 * @param time yyMMddHHmmss
	 *
	 *
	 * @return yyyy-MM-dd HH:mm:ss
	 */
	public static String changeBcd2NormalFormat(String time) {
		Date date = null;
		String dateStr = "";
		try {
			date = BCD_NORM_DATE_FORMAT.parse(time);
			dateStr = NORM_DATE_FORMAT.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dateStr;
	}

	/*
	 * 毫秒转化时分秒毫秒
	 */
	public static String formatTime(Long ms) {
		Integer ss = 1000;
		Integer mi = ss * 60;
		Integer hh = mi * 60;
		Integer dd = hh * 24;

		Long day = ms / dd;
		Long hour = (ms - day * dd) / hh;
		Long minute = (ms - day * dd - hour * hh) / mi;
		Long second = (ms - day * dd - hour * hh - minute * mi) / ss;
		Long milliSecond = ms - day * dd - hour * hh - minute * mi - second * ss;

		StringBuffer sb = new StringBuffer();
		if (day > 0) {
			sb.append(day + "天");
		}
		if (hour > 0) {
			sb.append(hour + "小时");
		}
		if (minute > 0) {
			sb.append(minute + "分");
		}
		if (second > 0) {
			sb.append(second + "秒");
		}
		if (milliSecond >= 0) {
			sb.append(milliSecond + "毫秒");
		}
		return sb.toString();
	}

	public static String date2Str(Date date) {
		return NORM_DATE_FORMAT.format(date);
	}

	public static String date2StrYYYYMMDD(Date date) {
		return YYYY_MM_DD_FORMAT.format(date);
	}

	/**
	 * 获取当前时间的前一天时间
	 *
	 * @param cl
	 * @return
	 */
	public static String getBeforeDay() {
		Calendar cl = Calendar.getInstance();
		int year = cl.get(Calendar.YEAR);
		int month = cl.get(Calendar.MONTH) + 1;
		int day = cl.get(Calendar.DATE) - 1;
		return year + "年" + (month < 10 ? ("0" + month) : month) + "月" + (day < 10 ? ("0" + day) : day) + "日";
	}

	/**
	 *
	 * @param date 指定的日期
	 * @param days 天数
	 * @return
	 */
	public static String getBeforeDays(Date date, int days) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		 calendar.add(Calendar.DATE, days);
		 return NORM_DATE_FORMAT.format(calendar.getTime());
	}

	public static String getBeforeDayDataStr() {
		Calendar cl = Calendar.getInstance();
		int year = cl.get(Calendar.YEAR);
		int month = cl.get(Calendar.MONTH);
		int day = cl.get(Calendar.DATE) - 1;
		cl.set(year, month, day);
		return NORM_DATE_FORMAT.format(cl.getTime());
	}

	public static String getBeforeDayDataStrYYYYMMDD() {
		Calendar cl = Calendar.getInstance();
		int year = cl.get(Calendar.YEAR);
		int month = cl.get(Calendar.MONTH);
		int day = cl.get(Calendar.DATE) - 1;
		cl.set(year, month, day);
		return YYYY_MM_DD_FORMAT.format(cl.getTime());
	}

	/**
	 * 获取当前时间的前一天时间 路径
	 *
	 * @param cl
	 * @return
	 */
	public static String getPdfGeneratePath() {
		Calendar cl = Calendar.getInstance();
		int year = cl.get(Calendar.YEAR);
		int month = cl.get(Calendar.MONTH) + 1;
		int day = cl.get(Calendar.DATE) - 1;
		return year + File.separator + (month < 10 ? ("0" + month) : month) + File.separator
				+ (day < 10 ? ("0" + day) : day) + File.separator;
	}

	public static String getPdfGeneratePath(String date) throws Exception {
		Date dateTime = null;
		dateTime = NORM_DATE_FORMAT.parse(date);
		Calendar cl = Calendar.getInstance();
		cl.setTime(dateTime);
		int year = cl.get(Calendar.YEAR);
		int month = cl.get(Calendar.MONTH) + 1;
		int day = cl.get(Calendar.DATE);
		return year + "//" + (month < 10 ? ("0" + month) : month) + "//" + (day < 10 ? ("0" + day) : day) + "//";
	}

	public static long hoursBetten2Days(String before, String after) throws Exception {
		Date date_before = null;
		Date date_after = null;
		date_before = NORM_DATE_FORMAT.parse(before);
		date_after = NORM_DATE_FORMAT.parse(after);
		long begin = date_before.getTime();
		long end = date_after.getTime();
		long diff = end - begin;
		return (diff) / (60 * 60 * 1000);
	}

	public static String date2String(Date date) {
		return YYYY_MM_DD_FORMAT.format(date);
	}

	public static String dateStrChange(String date) throws ParseException {
		Date d = YYYY_MM_DD_CHINESE_FORMAT.parse(date);
		return YYYY_MM_DD_FORMAT.format(d);
	}

	/**
	 *
	 * @param date yyyy年MM月
	 * @return
	 * @throws ParseException
	 */
	public static String getFirstDayOfMonth(String date) throws ParseException {
		Date d = YYYY_MM_CHINESE_FORMAT.parse(date);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(d);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
		String firstDayOfMonth = YYYY_MM_DD_FORMAT.format(calendar.getTime());
		return firstDayOfMonth;
	}

	public static String getLastDayOfMonth(String date) throws ParseException {
		Date d = YYYY_MM_CHINESE_FORMAT.parse(date);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(d);
		calendar.set(Calendar.DAY_OF_MONTH, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		String lastDayOfMonth = YYYY_MM_DD_FORMAT.format(calendar.getTime());
		return lastDayOfMonth;
	}

	/**
	 * 当前季度的开始时间
	 *
	 * @return
	 */
	public static String getQuarterStartTime(String year, String quarter) {
		Calendar c = Calendar.getInstance();
		c.set(Integer.parseInt(year), 1, 1);
		String now = null;
		try {
			if ("1".equals(quarter))
				c.set(Calendar.MONTH, 0);
			else if ("2".equals(quarter))
				c.set(Calendar.MONTH, 3);
			else if ("3".equals(quarter))
				c.set(Calendar.MONTH, 6);
			else if ("4".equals(quarter))
				c.set(Calendar.MONTH, 9);
			c.set(Calendar.DATE, 1);
			now = YYYY_MM_DD_FORMAT.format(c.getTime());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return now;
	}

	/**
	 * 当前季度的结束时间
	 *
	 * @return
	 */
	public static String getQuarterEndTime(String year, String quarter) {
		Calendar c = Calendar.getInstance();
		c.set(Integer.parseInt(year), 1, 1);
		String now = null;
		try {
			if ("1".equals(quarter)) {
				c.set(Calendar.MONTH, 2);
				c.set(Calendar.DATE, 31);
			} else if ("2".equals(quarter)) {
				c.set(Calendar.MONTH, 5);
				c.set(Calendar.DATE, 30);
			} else if ("3".equals(quarter)) {
				c.set(Calendar.MONTH, 8);
				c.set(Calendar.DATE, 30);
			} else if ("4".equals(quarter)) {
				c.set(Calendar.MONTH, 11);
				c.set(Calendar.DATE, 31);
			}
			now = YYYY_MM_DD_FORMAT.format(c.getTime());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return now;
	}

	public static String getYearStartTime(String y) {
		Calendar c = Calendar.getInstance();
		c.set(Integer.parseInt(y), 1, 1);
		c.set(Calendar.MONTH, 0);
		return YYYY_MM_DD_FORMAT.format(c.getTime());
	}

	public static String getYearEndTime(String y) {
		Calendar c = Calendar.getInstance();
		c.set(Integer.parseInt(y), 1, 1);
		c.set(Calendar.MONTH, 11);
		c.set(Calendar.DATE, 31);
		return YYYY_MM_DD_FORMAT.format(c.getTime());
	}

	public static String beforeOneHourToDate(String date) throws ParseException {
		Date d = NORM_DATE_FORMAT.parse(date);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(d);
		calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) - 1);
		System.out.println("一个小时前的时间：" + NORM_DATE_FORMAT.format(calendar.getTime()) + "\n 当前时间：" + date);
		return NORM_DATE_FORMAT.format(calendar.getTime());
	}

	public static String getLastYearString(Date date) throws Exception {
		Calendar ca = Calendar.getInstance();// 得到一个Calendar的实例
		ca.setTime(date); // 设置时间为当前时间
		ca.add(Calendar.YEAR, -1); // 年份减1
		// ca.add(Calendar.MONTH, -1);// 月份减1
		// ca.add(Calendar.DATE, -1);// 日期减1
		Date resultDate = ca.getTime(); // 结果
		return NORM_DATE_FORMAT.format(resultDate);
	}

	/**
	 * 获取上个月第一天
	 *
	 * @return
	 * @throws Exception
	 */
	public static String getLastMonthStart() throws Exception {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -1);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.set(Calendar.HOUR_OF_DAY, calendar.getActualMinimum(Calendar.HOUR_OF_DAY));
		// 设置每小时最大分钟
		calendar.set(Calendar.MINUTE, calendar.getActualMinimum(Calendar.MINUTE));
		// 设置每分钟最大秒
		calendar.set(Calendar.SECOND, calendar.getActualMinimum(Calendar.SECOND));
		Date resultDate = calendar.getTime(); // 结果
		return NORM_DATE_FORMAT.format(resultDate);
	}

	/**
	 * 获取上个月最后一天
	 *
	 * @return
	 * @throws Exception
	 */
	public static String getLastMonthEnd() throws Exception {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.add(Calendar.DATE, -1);
		calendar.set(Calendar.HOUR_OF_DAY, calendar.getActualMaximum(Calendar.HOUR_OF_DAY));
		// 设置每小时最大分钟
		calendar.set(Calendar.MINUTE, calendar.getActualMaximum(Calendar.MINUTE));
		// 设置每分钟最大秒
		calendar.set(Calendar.SECOND, calendar.getActualMaximum(Calendar.SECOND));
		Date resultDate = calendar.getTime(); // 结果
		return NORM_DATE_FORMAT.format(resultDate);
	}



	/**
	 * @title 获取上个月第一天
	 *
	 * @return
	 * @throws Exception
	 */
	public static String getLastMonth() throws Exception {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -1);
		Date resultDate = calendar.getTime(); // 结果
		return NORM_DATEMONTH_FORMAT.format(resultDate);
	}

	public static void main(String[] args) throws Exception {
		System.out.println(getLastMonth());
	}

}
