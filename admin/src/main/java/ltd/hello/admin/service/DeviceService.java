package ltd.hello.admin.service;

import ltd.hello.admin.model.Device;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author trik
 * @since 2021-04-09
 */
public interface DeviceService extends IService<Device> {

}
