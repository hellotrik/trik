package ltd.hello.admin.mapper;

import ltd.hello.admin.model.Orders;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 发布需求 Mapper 接口
 * </p>
 *
 * @author trik
 * @since 2021-04-09
 */
public interface OrdersMapper extends BaseMapper<Orders> {

}
