package ltd.hello.admin.service;

import ltd.hello.admin.model.Favorate;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 收藏 服务类
 * </p>
 *
 * @author trik
 * @since 2021-04-09
 */
public interface FavorateService extends IService<Favorate> {

}
