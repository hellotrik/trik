package ltd.hello.admin.service.impl;

import ltd.hello.admin.model.Equipment;
import ltd.hello.admin.mapper.EquipmentMapper;
import ltd.hello.admin.service.EquipmentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 设备 服务实现类
 * </p>
 *
 * @author trik
 * @since 2021-04-09
 */
@Service
public class EquipmentServiceImpl extends ServiceImpl<EquipmentMapper, Equipment> implements EquipmentService {

}
