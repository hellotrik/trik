package ltd.hello.admin.service.impl;

import ltd.hello.admin.model.Orders;
import ltd.hello.admin.mapper.OrdersMapper;
import ltd.hello.admin.service.OrdersService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 发布需求 服务实现类
 * </p>
 *
 * @author trik
 * @since 2021-04-09
 */
@Service
public class OrdersServiceImpl extends ServiceImpl<OrdersMapper, Orders> implements OrdersService {

}
