package ltd.hello.trik.service;


import com.baomidou.mybatisplus.extension.service.IService;
import ltd.hello.trik.model.Wxuser;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author nianyy
 * @since 2019-11-13
 */
public interface WxuserService extends IService<Wxuser> {


    Wxuser selectByOpenId(String oepnId);
}
