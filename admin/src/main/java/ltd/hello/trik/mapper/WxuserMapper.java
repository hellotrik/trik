package ltd.hello.trik.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import ltd.hello.trik.model.Wxuser;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author nianyy
 * @since 2019-11-13
 */
public interface WxuserMapper extends BaseMapper<Wxuser> {

}
