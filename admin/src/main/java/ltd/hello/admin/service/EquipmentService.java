package ltd.hello.admin.service;

import ltd.hello.admin.model.Equipment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 设备 服务类
 * </p>
 *
 * @author trik
 * @since 2021-04-09
 */
public interface EquipmentService extends IService<Equipment> {

}
