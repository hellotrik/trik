package ltd.hello.trik.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import ltd.hello.trik.mapper.LogMapper;
import ltd.hello.trik.model.LogEntity;
import ltd.hello.trik.service.LogService;
import org.springframework.stereotype.Service;

@Service("logService")
public class LogServiceImpl extends ServiceImpl<LogMapper, LogEntity> implements LogService {
}
