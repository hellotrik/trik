package ltd.hello.admin.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 
 * </p>
 *
 * @author trik
 * @since 2021-04-09
 */
@Data
  @EqualsAndHashCode(callSuper = false)
    @TableName("project")
@ApiModel(value="Project对象", description="")
public class Project implements Serializable {

    private static final long serialVersionUID=1L;

      @TableId(value = "Id", type = IdType.AUTO)
      private Integer Id;

      @ApiModelProperty(value = "项目名称")
      private String name;

      @ApiModelProperty(value = "负责人")
      private String leader;

      @ApiModelProperty(value = "来源")
      private String funding;

      @ApiModelProperty(value = "承担单位")
      private String undertaking;

      @ApiModelProperty(value = "所属领域")
      private String field;

      @ApiModelProperty(value = "联系人")
      private String linkman;

      @ApiModelProperty(value = "联系电话")
      private String contact;

      @ApiModelProperty(value = "简介（预留）")
      private String remark;

      @ApiModelProperty(value = "登记时间")
      private Date createTime;

    private String recorder;


}
