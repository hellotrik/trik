package ltd.hello.admin.service;

import ltd.hello.admin.model.Orders;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 发布需求 服务类
 * </p>
 *
 * @author trik
 * @since 2021-04-09
 */
public interface OrdersService extends IService<Orders> {

}
