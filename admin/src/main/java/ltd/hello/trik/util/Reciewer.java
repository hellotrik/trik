package ltd.hello.trik.util;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

public class Reciewer {

	public static String deleteData(String url) {
		URL urlStr = null;
		String res = "";
		try {
			urlStr = new URL(url);
			HttpURLConnection connection = (HttpURLConnection) urlStr.openConnection();
			connection.setRequestMethod("DELETE");
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setRequestProperty("content-type", "text/html");
			// OutputStreamWriter out = new
			// OutputStreamWriter(connection.getOutputStream(), "UTF-8");
			// out.write(param);
			// out.flush();
			// out.close();
			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			String line = null;
			StringBuffer content = new StringBuffer();
			while ((line = in.readLine()) != null) {
				content.append(line);
			}
			res = content.toString();
			if (in != null) {
				in.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return res;
	}

	public static String getData(String path) throws IOException {
		BufferedReader in = null;
		StringBuilder result = new StringBuilder();
		try {
			URL url = new URL(path);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			// Get请求不需要DoOutPut
			conn.setDoOutput(false);
			conn.setDoInput(true);
			// 设置连接超时时间和读取超时时间
			conn.setConnectTimeout(60000);
			conn.setReadTimeout(60000);
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			// 连接服务器
			conn.connect();
			// 取得输入流，并使用Reader读取
			in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
			String line;
			while ((line = in.readLine()) != null) {
				result.append(line);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return result.toString();
	}

	public static String getData(String path, Map<String, String> headmap) throws IOException {
		BufferedReader in = null;
		StringBuilder result = new StringBuilder();
		try {
			URL url = new URL(path);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			// Get请求不需要DoOutPut
			conn.setDoOutput(false);
			conn.setDoInput(true);
			// 设置连接超时时间和读取超时时间
			conn.setConnectTimeout(10000);
			conn.setReadTimeout(10000);
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			for (Map.Entry<String, String> entry : headmap.entrySet()) {
				conn.setRequestProperty(entry.getKey(), entry.getValue());
			}
			// 连接服务器
			conn.connect();
			// 取得输入流，并使用Reader读取
			in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
			String line;
			while ((line = in.readLine()) != null) {
				result.append(line);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (in != null) {
					in.close();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return result.toString();
	}

	public static String postData(String url1, String param) throws IOException {
		// 创建指定url的url对象
		URL url = new URL(url1);
		// 创建http链接对象
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		// 设置准备进行输入、输出
		con.setDoOutput(true);
		con.setDoInput(true);
		// 设置请求方式
		con.setRequestMethod("POST");
		// 设置一般请求属性!!!!!!!!!之前没有写这句报错415
		con.setRequestProperty("Content-Type", "application/json");
		// 连接
		con.connect();
		// 创建输出流并设置编码为UTF-8
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(con.getOutputStream(),
				"UTF-8"));
		// 往输出流里传请求参数
		writer.write(param);
		writer.flush();
		// 打开链接

		// 定义输入流来读取URL的响应并设置编码为UTF-8
		BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream(),
				"UTF-8"));
		// 将返回数据拼接为字符串
		StringBuffer sb = new StringBuffer();
		String temp = null;
		// 获取数据
		while ((temp = reader.readLine()) != null) {
			sb.append(temp);
		}
		// 关闭流
		if (writer != null) {
			writer.close();
		}
		if (reader != null) {
			reader.close();
		}
		con.disconnect();
		// System.out.println(sb.toString());
		return sb.toString();
	}

	public static String postData(String url1, String param, Map<String, String> headmap) throws IOException {
		// 创建指定url的url对象
		URL url = new URL(url1);
		// 创建http链接对象
		HttpURLConnection con = (HttpURLConnection) url.openConnection();
		// 设置准备进行输入、输出
		con.setDoOutput(true);
		con.setDoInput(true);
		// 设置请求方式
		con.setRequestMethod("POST");
		// 设置一般请求属性!!!!!!!!!之前没有写这句报错415
		con.setRequestProperty("Content-Type", "application/json");
		for (Map.Entry<String, String> entry : headmap.entrySet()) {
			con.setRequestProperty(entry.getKey(), entry.getValue());
		}
		// 连接
		con.connect();
		// 创建输出流并设置编码为UTF-8
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(con.getOutputStream(),
				"UTF-8"));
		// 往输出流里传请求参数
		writer.write(param);
		writer.flush();
		// 打开链接

		// 定义输入流来读取URL的响应并设置编码为UTF-8
		BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream(),
				"UTF-8"));
		// 将返回数据拼接为字符串
		StringBuffer sb = new StringBuffer();
		String temp = null;
		// 获取数据
		while ((temp = reader.readLine()) != null) {
			sb.append(temp);
		}
		// 关闭流
		if (writer != null) {
			writer.close();
		}
		if (reader != null) {
			reader.close();
		}
		con.disconnect();
		// System.out.println(sb.toString());
		return sb.toString();
	}

	public static void main(String[] args) throws IOException {
		Reciewer.postData(
				"http://192.168.1.238:8081/link/insert",
				"{\"type\":1,\"platformId\":12,\"platformName\":\"13123\",\"upTime\":\"2017-11-21 15:00:00\",\"disconnTime\":\"2017-11-21 15:30:00\",\"duration\":30,\"positionNum\":20,\"alarmNum\":12,\"errorNum\":321}");
	}

}
