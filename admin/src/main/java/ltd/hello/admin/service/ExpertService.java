package ltd.hello.admin.service;

import ltd.hello.admin.model.Expert;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author trik
 * @since 2021-04-09
 */
public interface ExpertService extends IService<Expert> {

}
