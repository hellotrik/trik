package ltd.hello.admin.mapper;

import ltd.hello.admin.model.Innovation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author trik
 * @since 2021-04-09
 */
public interface InnovationMapper extends BaseMapper<Innovation> {

}
