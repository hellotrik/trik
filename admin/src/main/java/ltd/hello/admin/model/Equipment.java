package ltd.hello.admin.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 设备
 * </p>
 *
 * @author trik
 * @since 2021-04-09
 */
@Data
  @EqualsAndHashCode(callSuper = false)
    @TableName("equipment")
@ApiModel(value="Equipment对象", description="设备")
public class Equipment implements Serializable {

    private static final long serialVersionUID=1L;

      @TableId(value = "Id", type = IdType.AUTO)
      private Integer Id;

    private String name;

    private String deviceId;

    private String deviceSpec;

    private String productFactory;

    private String deviceType;

      @ApiModelProperty(value = "所属单位")
      private String org;

      @ApiModelProperty(value = "所属部门")
      private String dept;

    private String seviceArea;

    private String contactPerson;

    private String contactInfo;

      @ApiModelProperty(value = "开放时间")
      private String openTime;

    private String keyWords;

    private Date createTime;

    private String remark;

    private String recorder;


}
