package ltd.hello.trik.feign.fallback;


import ltd.hello.trik.feign.FeignTestService;

public class FeignTestFallback implements FeignTestService {

    @Override
    public String search(String wd) {
        return null;
    }
}
