package ltd.hello.trik.model;

import com.alibaba.fastjson.annotation.JSONField;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author nianyy
 * @since 2019-11-13
 */
@EqualsAndHashCode(callSuper = false)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Wxuser extends Model<Wxuser> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "Id", type = IdType.AUTO)
    private Integer Id;

    /**
     * 姓名
     */
    private String name;

    /**
     * 电话
     */
    private String mobile;

    /**
     * 工作单位
     */
    private String workOrg;

    /**
     * 所在部门
     */
    private String workDept;

    /**
     * 备注
     */
    private String remark;

    private String openid;

    private String address;

    private String status;

    private String createUser;

    private String updateUser;

    private String avatarUrl;
    private String city;
    private String country;
    private String nickName;
    private String province;
    private byte gender;

    /**
     * 登记时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss") // FastJson包使用注解
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8") // Jackson包使用注解
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") // 格式化前台日期参数注解
    @TableField("create_time")
    private Date createTime;
    /**
     * 登记时间
     */
    @JSONField(format = "yyyy-MM-dd HH:mm:ss") // FastJson包使用注解
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8") // Jackson包使用注解
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") // 格式化前台日期参数注解
    @TableField("update_time")
    private Date updateTime;


    @Override
    protected Serializable pkVal() {
        return this.Id;
    }
}
