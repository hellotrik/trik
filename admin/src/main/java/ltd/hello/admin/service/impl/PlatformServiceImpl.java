package ltd.hello.admin.service.impl;

import ltd.hello.admin.model.Platform;
import ltd.hello.admin.mapper.PlatformMapper;
import ltd.hello.admin.service.PlatformService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 平台 服务实现类
 * </p>
 *
 * @author trik
 * @since 2021-04-09
 */
@Service
public class PlatformServiceImpl extends ServiceImpl<PlatformMapper, Platform> implements PlatformService {

}
