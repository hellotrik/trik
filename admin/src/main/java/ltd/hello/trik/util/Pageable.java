package ltd.hello.trik.util;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Pageable {

	private int total;// 记录总数
	private int pageCount;// 分页总数
	private Object list;// 分页记录集合
	private int pageSize; // 每页大小
	private int pageIndex; // 当前页码

	public Pageable(int pageSize) {
		this.pageSize = pageSize;
	}

	public void setTotal(int total) {
		this.total = total;
		this.pageCount = total % pageSize == 0 ? total / pageSize : total / pageSize + 1;
	}


}
