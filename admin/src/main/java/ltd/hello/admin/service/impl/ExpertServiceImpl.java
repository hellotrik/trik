package ltd.hello.admin.service.impl;

import ltd.hello.admin.model.Expert;
import ltd.hello.admin.mapper.ExpertMapper;
import ltd.hello.admin.service.ExpertService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author trik
 * @since 2021-04-09
 */
@Service
public class ExpertServiceImpl extends ServiceImpl<ExpertMapper, Expert> implements ExpertService {

}
