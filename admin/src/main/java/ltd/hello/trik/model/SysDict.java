package ltd.hello.trik.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author houzw
 * @since 2019-08-06
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SysDict implements Serializable {

    private static final long serialVersionUID = 1L;


    private Integer Id;

    /**
     * 字典编码
     */

    private String dictCode;

    /**
     * 父编码
     */

    private String parentCode;

    /**
     * 编码名称
     */

    private String dictName;

    /**
     * 类型
     */

    private String dictType;


    private String typeInfo;


    private String remark;

    private String isValid;


	private Date createTime;

    private String recorder;
}
