package ltd.hello.trik.model;

import com.alibaba.fastjson.annotation.JSONField;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

@TableName("log")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LogEntity {

	/** 描述 (@author: houzw) */

	private static final long serialVersionUID = 1L;
	@TableId(type = IdType.AUTO)
	private Long id;
	private Long userid;
	private String username;
	private String operation;
	private Long time;
	private String method;
	private String params;
	private String ip;
	private String openid;
	@JSONField(format = "yyyy-MM-dd HH:mm:ss") // FastJson包使用注解
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8") // Jackson包使用注解
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") // 格式化前台日期参数注解
	private String logTime;

	public void setUsername(String username) {
		this.username = username == null ? null : username.trim();
	}

	public void setOperation(String operation) {
		this.operation = operation == null ? null : operation.trim();
	}

	public void setMethod(String method) {
		this.method = method == null ? null : method.trim();
	}

	public void setParams(String params) {
		this.params = params == null ? null : params.trim();
	}

	public void setIp(String ip) {
		this.ip = ip == null ? null : ip.trim();
	}

	protected Serializable pkVal() {
		return this.id;
	}
}
